﻿// <copyright file="PersonsDBContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankingAppDB
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using BankingAppDB.Models;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.ChangeTracking;
    using Microsoft.EntityFrameworkCore.SqlServer;

    /// <summary>
    /// Creation of DB Context.
    /// </summary>
    ///
    public class PersonsDBContext : DbContext
    {
        private const string DefaultConStr = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\BankingDB.mdf;Integrated Security=True";

        /// <summary>
        /// Gets or sets person table.
        /// </summary>
        public DbSet<Person> Persons { get; set; }

        /// <summary>
        /// Gets or sets account table.
        /// </summary>
        public DbSet<Account> Accounts { get; set; }

        /// <summary>
        /// Gets or sets transaction table.
        /// </summary>
        public DbSet<Transaction> Transactions { get; set; }

        /// <summary>
        /// Gets or sets person - account connection table.
        /// </summary>
        public DbSet<PersonAccount> PersonAccounts { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonsDBContext"/> class.
        /// Ensure that DB is created.
        /// </summary>
        public PersonsDBContext()
            : base()
        {
            Database.EnsureCreated();
        }

        // * DBContext constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonsDBContext"/> class.
        /// </summary>
        /// <param name="options">Options.</param>
        public PersonsDBContext(DbContextOptions<PersonsDBContext> options)

            : base(options)
        {
        }

        // * Configuration + add Server and LazyLoading

        /// <inheritdoc/>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder != null && !optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseLazyLoadingProxies()
                              .UseSqlServer(DefaultConStr);
            }
        }

        /// <summary>
        /// Building the database modell, the connections.
        /// </summary>
        /// <param name="modelBuilder">modelBuilder.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            if (modelBuilder != null)
            {
                modelBuilder.Entity<PersonAccount>()
                .HasKey(pa => new { pa.AccountId, pa.PersonId });

                modelBuilder.Entity<PersonAccount>()
                    .HasOne(pa => pa.Personn)
                    .WithMany(p => p.Personaccount)
                    .HasForeignKey(pa => pa.PersonId);

                modelBuilder.Entity<PersonAccount>()
               .HasOne(pa => pa.Accountt)
               .WithMany(a => a.Personaccount)
               .HasForeignKey(pa => pa.AccountId);

                modelBuilder.Entity<Transaction>()
                    .HasOne(t => t.Accounts)
                    .WithMany(a => a.Transactions)
                    .HasForeignKey(t => t.AccountId);

                /*modelBuilder.Entity<Person>(person =>
                {
                    person.OwnsMany(p => p.Accounts)
                          .OwnsMany(a => a.Persons)
                          .HasKey(a => a.Accounts);
                });


                /*modelBuilder.Entity<Account>(account =>
                {
                    account.OwnsMany(a => a.Persons)
                          .OwnsMany(p => p.Accounts)
                          .HasKey(a => a.Persons);
                });*/
                /*modelBuilder.Entity<Transaction>(transaction =>
                {
                    transaction.HasOne(t => t.Accounts)
                          .WithMany(a => a.Transactions)
                          ;
                });*/

                /*modelBuilder.Entity<PersonAccount>().HasKey(pa => new { pa.accountd, pa.personId });

                modelBuilder.Entity<PersonAccount>()
                    .HasOne<Person>(pa => pa.person)
                    .WithMany(p => p.personaccount)
                    .HasForeignKey(p => p.personId);

                modelBuilder.Entity<PersonAccount>()
                    .HasOne<Account>(pa => pa.account)
                    .WithMany(p => p.personaccount)
                    .HasForeignKey(p => p.account
            modelBuilder.Entity<PersonAccount>()
                .HasKey(pa => new { pa.AccountId, pa.PersonId });

                modelBuilder.Entity<PersonAccount>()
                    .HasOne(pa => pa.person)
                    .WithMany(p => p.personaccount)
                    .HasForeignKey(pa => pa.PersonId);


                 modelBuilder.Entity<PersonAccount>()
                .HasOne(pa => pa.account)
                .WithMany(a => a.personaccount)
                .HasForeignKey(pa => pa.AccountId);

                modelBuilder.Entity<Transaction>()
                    .HasOne(t => t.Accounts)
                    .WithMany(a => a.Transactions)
                    .HasForeignKey(t => t.AccountId);



           /* modelBuilder.Entity<Account>()
                .HasMany(pa => pa.personaccount)
                .WithRequired()
                .HasForeignKey(pa=> pa.AccounId);*/

                /* modelBuilder.Entity<Transaction>()
                     .OwnsOne<Account>(t => t.Accounts)
                     .WithOwner(a => a.;

                     .HasOne<Account>(t => t.Accounts)
                     .*/
            }

            /*Account a1 = new Account()
            {
                Id = "001",
                Name = "MyAccount"

            };

            Person p1 = new Person()
            {
                Id = "001",
                Név = "András"
            };

            Transaction t1 = new Transaction() { Id = "001", TransferTime = DateTime.Now, Amount = 100000, };
            Transaction t2 = new Transaction() { Id = "002", TransferTime = DateTime.Now, Amount = 200000, };

            if (modelBuilder != null )
            {
                modelBuilder.Entity<Transaction>().HasData(t1, t2);
               modelBuilder.Entity<Person>().HasData(p1);
                modelBuilder.Entity<Account>().HasData(a1);

            }*/
        }
        }
    }
