﻿// <copyright file="Transaction.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankingAppDB.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// The model class of the transaction.
    /// </summary>
    public class Transaction
    {
        // Models for the transactions

        /// <summary>
        /// Gets or sets id.
        /// </summary>
        [Key]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the type of the trasaction.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the time of sending the transaction.
        /// </summary>
        public DateTime TransferTime { get; set; }

        /// <summary>
        /// Gets or sets transaction amount.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Gets or sets currency of transacrótion.
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Gets or sets id of the account.
        /// </summary>
        public string AccountId { get; set; }

        /// <summary>
        /// Gets or sets the correponding Account.
        /// </summary>
        [NotMapped]
        public virtual Account Accounts { get; set; }

        /// <summary>
        /// To String method for transaction.
        /// </summary>
        /// <returns>Accounts.</returns>
        public override string ToString()
        {
            return "Azonosító: " + this.Id + this.AccountId + " penzosszeg: " + this.Amount + " valuta: " + this.Currency + " dátum: " + this.TransferTime;
         }
    }
}
