﻿// <copyright file="PersonAccount.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankingAppDB.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// Modell for Person-Account connection.
    /// </summary>
    public class PersonAccount
    {
        /// <summary>
        ///  Gets or sets personId.
        /// </summary>
        [Key]

        public string PersonId { get; set; }

        /// <summary>
        ///  Gets or sets accountId.
        /// </summary>
        [Key]

        public string AccountId { get; set; }

        /// <summary>
        /// Gets or sets navigation property account.
        /// </summary>
        [ForeignKey("Account")]
        public virtual Account Accountt { get; set; }

        /// <summary>
        /// Gets or sets navigation property person.
        /// </summary>
        [ForeignKey("Person")]
        public virtual Person Personn { get; set; }

        /// <summary>
        /// To string method.
        /// </summary>
        /// <returns>Persons.</returns>
        public override string ToString()
        {
            return this.PersonId + " " + this.AccountId;
        }
    }
}
