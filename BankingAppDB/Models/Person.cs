﻿// <copyright file="Person.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankingAppDB.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// The prorepties of the person table.
    /// </summary>
    public class Person
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Person"/> class.
        /// List of person accounts to navigation property.
        /// </summary>
        public Person()
        {
            this.Personaccount = new HashSet<PersonAccount>();
        }

        /// <summary>
        /// Gets or sets id of the peron.
        /// </summary>
        [Key]

        // *[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets person id.
        /// </summary>
        public string Név { get; set; }

        /// <summary>
        /// Gets or sets date of Birth.
        /// </summary>
        public DateTime SZülidő { get; set; }

        /// <summary>
        /// Gets or sets name of mother.
        /// </summary>
        public string AnyjaNeve { get; set; }

        /// <summary>
        /// Gets or sets address.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets accountid.
        /// </summary>
        public int AccountId { get; set; }

        /// <summary>
        /// Gets the corresponding Accounts.
        /// </summary>
        ///
        [NotMapped]
        public virtual ICollection<PersonAccount> Personaccount { get; }

        /// <summary>
        /// To String method.
        /// </summary>
        /// <returns>PersonAccount.</returns>
        public override string ToString()
        {
            return this.Id + " " + this.Név;
        }
    }
}
