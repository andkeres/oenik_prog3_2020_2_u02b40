﻿// <copyright file="Account.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankingAppDB.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// The database description of an account.
    /// </summary>
    public class Account
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Account"/> class.
        /// Account constructor.
        /// </summary>
        public Account()
        {
            this.Personaccount = new HashSet<PersonAccount>();
        }

        /// <summary>
        /// Gets or sets id of the account.
        /// </summary>
        [Key]

        // *[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets name of the account.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether inactive flag of the account.
        /// </summary>
        public bool IsInactive { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether closed flag of the account.
        /// </summary>
        public bool IsClosed { get; set; }

        /// <summary>
        /// Gets or sets type of the account.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets the persons assigned to the account.
        /// </summary>

        /// <summary>
        /// The personaccounts to the account.
        /// </summary>
        [NotMapped]

        public virtual ICollection<PersonAccount> Personaccount { get; }

        // *public virtual ICollection<Person> Persons { get; set; }

        /// <summary>
        /// Gets the transactions assigned to the account
        ///
        /// The Transactions of the account.
        /// </summary>
        [NotMapped]

        public virtual ICollection<Transaction> Transactions { get; }

        /// <summary>
        /// ToString methos for the Account.
        /// </summary>
        /// <returns>TransactionList.</returns>
        public override string ToString()
        {
            return this.Id + " " + this.Name;
        }
    }
}
