var searchData=
[
  ['main_76',['Main',['../class_banking_appplication_1_1_program.html#ab48673cf683236aecce1c1e415b2fca1',1,'BankingAppplication::Program']]],
  ['menupoint_77',['Menupoint',['../class_banking_appplication_1_1_program.html#ab9135e89102cf193488a959969c214fd',1,'BankingAppplication::Program']]],
  ['modifyaccount_78',['ModifyAccount',['../class_banking_appplication_1_1_program.html#ab9135e89102cf193488a959969c214fda2cc61b24aea4bfd22503834903aff64f',1,'BankingAppplication::Program']]],
  ['modifyaccountname_79',['ModifyAccountName',['../class_bank_app_logic_1_1_change_data.html#a5393aefc1afb3b87aec279095c28295b',1,'BankAppLogic.ChangeData.ModifyAccountName()'],['../interface_bank_app_logic_1_1_i_change_data.html#afb622d810b22409f17fbb7f7ea9f8562',1,'BankAppLogic.IChangeData.ModifyAccountName()']]],
  ['modifypersonaddress_80',['ModifyPersonAddress',['../class_bank_app_logic_1_1_change_data.html#a72a05b4e9cbd92e022d00f69df695e9b',1,'BankAppLogic.ChangeData.ModifyPersonAddress()'],['../interface_bank_app_logic_1_1_i_change_data.html#ae35491ed6c893683d76ee0af29d849de',1,'BankAppLogic.IChangeData.ModifyPersonAddress()']]],
  ['modifypersonname_81',['ModifyPersonName',['../class_bank_app_logic_1_1_change_data.html#ab54a20417d9852121ef8ca2e4d919f4f',1,'BankAppLogic.ChangeData.ModifyPersonName()'],['../interface_bank_app_logic_1_1_i_change_data.html#a29369a8c635e8860edc9540349255f66',1,'BankAppLogic.IChangeData.ModifyPersonName()']]],
  ['modifypersonnameisworkingasexpected_82',['ModifyPersonNameIsWorkingAsExpected',['../class_teszt_1_1_test_change_data.html#a9d9d0819ac44c50655189441874e70cb',1,'Teszt::TestChangeData']]],
  ['modifytransactionamount_83',['ModifyTransactionAmount',['../class_bank_app_logic_1_1_change_data.html#ad08e540d642714e7b61f184094fae362',1,'BankAppLogic.ChangeData.ModifyTransactionAmount()'],['../interface_bank_app_logic_1_1_i_change_data.html#a4d73e947c8af58481cad96335eb774aa',1,'BankAppLogic.IChangeData.ModifyTransactionAmount()']]]
];
