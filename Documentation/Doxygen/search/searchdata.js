var indexSectionsWithContent =
{
  0: "abcdfgimnopqrstu",
  1: "abcdinopqt",
  2: "bt",
  3: "abcdfgmnopstu",
  4: "ad",
  5: "m",
  6: "acmn",
  7: "acinpst",
  8: "r"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "properties",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Properties",
  8: "Pages"
};

