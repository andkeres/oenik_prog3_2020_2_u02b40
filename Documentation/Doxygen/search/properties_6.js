var searchData=
[
  ['transactionid_259',['Transactionid',['../class_bank_app_logic_1_1_models_1_1_account_to_transaction.html#ae7fad399d5c4882ba955189a3a833912',1,'BankAppLogic::Models::AccountToTransaction']]],
  ['transactions_260',['Transactions',['../class_banking_app_d_b_1_1_models_1_1_account.html#a295d6ee21b4898790308482227bfa25c',1,'BankingAppDB.Models.Account.Transactions()'],['../class_banking_app_d_b_1_1_persons_d_b_context.html#ae78bfe2e0e352cb01393a85d89616026',1,'BankingAppDB.PersonsDBContext.Transactions()']]],
  ['transfertime_261',['TransferTime',['../class_banking_app_d_b_1_1_models_1_1_transaction.html#a99b6e8ca13ca0840018add4d29474a35',1,'BankingAppDB::Models::Transaction']]],
  ['type_262',['Type',['../class_banking_app_d_b_1_1_models_1_1_account.html#a0b4ed6944edcebff8a5f79eb4c8cfc68',1,'BankingAppDB.Models.Account.Type()'],['../class_banking_app_d_b_1_1_models_1_1_transaction.html#a37af98fa114f779bfbbb78608a2cfdad',1,'BankingAppDB.Models.Transaction.Type()']]]
];
