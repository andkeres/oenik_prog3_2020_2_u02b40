var namespace_bank_app_logic =
[
    [ "Models", "namespace_bank_app_logic_1_1_models.html", "namespace_bank_app_logic_1_1_models" ],
    [ "ChangeData", "class_bank_app_logic_1_1_change_data.html", "class_bank_app_logic_1_1_change_data" ],
    [ "DataQuery", "class_bank_app_logic_1_1_data_query.html", "class_bank_app_logic_1_1_data_query" ],
    [ "IChangeData", "interface_bank_app_logic_1_1_i_change_data.html", "interface_bank_app_logic_1_1_i_change_data" ],
    [ "IDataQuery", "interface_bank_app_logic_1_1_i_data_query.html", "interface_bank_app_logic_1_1_i_data_query" ],
    [ "INewAccount", "interface_bank_app_logic_1_1_i_new_account.html", "interface_bank_app_logic_1_1_i_new_account" ],
    [ "INewTransaction", "interface_bank_app_logic_1_1_i_new_transaction.html", "interface_bank_app_logic_1_1_i_new_transaction" ],
    [ "NewAccount", "class_bank_app_logic_1_1_new_account.html", "class_bank_app_logic_1_1_new_account" ],
    [ "NewTransaction", "class_bank_app_logic_1_1_new_transaction.html", "class_bank_app_logic_1_1_new_transaction" ]
];