var class_bank_app_logic_1_1_data_query =
[
    [ "DataQuery", "class_bank_app_logic_1_1_data_query.html#a77c74214ad456d69a25e3469c81d52e1", null ],
    [ "GetAllAccount", "class_bank_app_logic_1_1_data_query.html#a1a20deb314f01d81211b5fbedf1d7d25", null ],
    [ "GetAllPersons", "class_bank_app_logic_1_1_data_query.html#a10d0f0355de4e06fe882d0a0da8ea4df", null ],
    [ "GetPersonToAccount", "class_bank_app_logic_1_1_data_query.html#a313faf4006d5f2f25b266129656d8eef", null ],
    [ "GetTransactions", "class_bank_app_logic_1_1_data_query.html#a4ea6241a9745b9242c193f1c64e661b5", null ],
    [ "SumofAPeriod", "class_bank_app_logic_1_1_data_query.html#ab09c19f1259bd27977250fc5e89ea04f", null ],
    [ "TransactionAccountRelations", "class_bank_app_logic_1_1_data_query.html#a9d245ab5021f466a5e84dd700c791261", null ],
    [ "TransactionForAnAccount", "class_bank_app_logic_1_1_data_query.html#ac96014ccff59b6e52f66792558359697", null ],
    [ "TransactionsOverTreshold", "class_bank_app_logic_1_1_data_query.html#a48991878f58fdf225ad4033570a68ac9", null ]
];