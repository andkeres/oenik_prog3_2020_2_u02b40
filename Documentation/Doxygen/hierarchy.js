var hierarchy =
[
    [ "BankingAppDB.Models.Account", "class_banking_app_d_b_1_1_models_1_1_account.html", null ],
    [ "BankAppLogic.Models.AccountToTransaction", "class_bank_app_logic_1_1_models_1_1_account_to_transaction.html", null ],
    [ "BankingAppRepository2.Repositorii.BaseRepository< Account >", "class_banking_app_repository2_1_1_repositorii_1_1_base_repository.html", [
      [ "BankingAppRepository2.Repositorii.AccountRepository", "class_banking_app_repository2_1_1_repositorii_1_1_account_repository.html", null ]
    ] ],
    [ "BankingAppRepository2.Repositorii.BaseRepository< Person >", "class_banking_app_repository2_1_1_repositorii_1_1_base_repository.html", [
      [ "BankingAppRepository2.Repositorii.PersonRepository", "class_banking_app_repository2_1_1_repositorii_1_1_person_repository.html", null ]
    ] ],
    [ "BankingAppRepository2.Repositorii.BaseRepository< PersonAccount >", "class_banking_app_repository2_1_1_repositorii_1_1_base_repository.html", [
      [ "BankingAppRepository2.Repositorii.PersonAccountRepository", "class_banking_app_repository2_1_1_repositorii_1_1_person_account_repository.html", null ]
    ] ],
    [ "BankingAppRepository2.Repositorii.BaseRepository< Transaction >", "class_banking_app_repository2_1_1_repositorii_1_1_base_repository.html", [
      [ "BankingAppRepository2.Repositorii.TransactionRepository", "class_banking_app_repository2_1_1_repositorii_1_1_transaction_repository.html", null ]
    ] ],
    [ "DbContext", null, [
      [ "BankingAppDB.PersonsDBContext", "class_banking_app_d_b_1_1_persons_d_b_context.html", null ]
    ] ],
    [ "BankAppLogic.IChangeData", "interface_bank_app_logic_1_1_i_change_data.html", [
      [ "BankAppLogic.ChangeData", "class_bank_app_logic_1_1_change_data.html", null ]
    ] ],
    [ "BankAppLogic.IDataQuery", "interface_bank_app_logic_1_1_i_data_query.html", [
      [ "BankAppLogic.DataQuery", "class_bank_app_logic_1_1_data_query.html", null ]
    ] ],
    [ "BankAppLogic.INewAccount", "interface_bank_app_logic_1_1_i_new_account.html", [
      [ "BankAppLogic.NewAccount", "class_bank_app_logic_1_1_new_account.html", null ]
    ] ],
    [ "BankAppLogic.INewTransaction", "interface_bank_app_logic_1_1_i_new_transaction.html", [
      [ "BankAppLogic.NewTransaction", "class_bank_app_logic_1_1_new_transaction.html", null ]
    ] ],
    [ "BankingAppRepository2.Repositorii.IRepository< T >", "interface_banking_app_repository2_1_1_repositorii_1_1_i_repository.html", [
      [ "BankingAppRepository2.Repositorii.BaseRepository< T >", "class_banking_app_repository2_1_1_repositorii_1_1_base_repository.html", null ]
    ] ],
    [ "BankingAppRepository2.Repositorii.IRepository< Account >", "interface_banking_app_repository2_1_1_repositorii_1_1_i_repository.html", [
      [ "BankingAppRepository2.Repositorii.IAccountRepository", "interface_banking_app_repository2_1_1_repositorii_1_1_i_account_repository.html", [
        [ "BankingAppRepository2.Repositorii.AccountRepository", "class_banking_app_repository2_1_1_repositorii_1_1_account_repository.html", null ]
      ] ]
    ] ],
    [ "BankingAppRepository2.Repositorii.IRepository< BankingAppDB.Models.Account >", "interface_banking_app_repository2_1_1_repositorii_1_1_i_repository.html", null ],
    [ "BankingAppRepository2.Repositorii.IRepository< BankingAppDB.Models.Person >", "interface_banking_app_repository2_1_1_repositorii_1_1_i_repository.html", null ],
    [ "BankingAppRepository2.Repositorii.IRepository< BankingAppDB.Models.PersonAccount >", "interface_banking_app_repository2_1_1_repositorii_1_1_i_repository.html", null ],
    [ "BankingAppRepository2.Repositorii.IRepository< BankingAppDB.Models.Transaction >", "interface_banking_app_repository2_1_1_repositorii_1_1_i_repository.html", null ],
    [ "BankingAppRepository2.Repositorii.IRepository< Person >", "interface_banking_app_repository2_1_1_repositorii_1_1_i_repository.html", [
      [ "BankingAppRepository2.Repositorii.IPersonRepository", "interface_banking_app_repository2_1_1_repositorii_1_1_i_person_repository.html", [
        [ "BankingAppRepository2.Repositorii.PersonRepository", "class_banking_app_repository2_1_1_repositorii_1_1_person_repository.html", null ]
      ] ]
    ] ],
    [ "BankingAppRepository2.Repositorii.IRepository< PersonAccount >", "interface_banking_app_repository2_1_1_repositorii_1_1_i_repository.html", [
      [ "BankingAppRepository2.Repositorii.IPersonAccount", "interface_banking_app_repository2_1_1_repositorii_1_1_i_person_account.html", [
        [ "BankingAppRepository2.Repositorii.PersonAccountRepository", "class_banking_app_repository2_1_1_repositorii_1_1_person_account_repository.html", null ]
      ] ]
    ] ],
    [ "BankingAppRepository2.Repositorii.IRepository< Transaction >", "interface_banking_app_repository2_1_1_repositorii_1_1_i_repository.html", [
      [ "BankingAppRepository2.Repositorii.ITransactionRepository", "interface_banking_app_repository2_1_1_repositorii_1_1_i_transaction_repository.html", [
        [ "BankingAppRepository2.Repositorii.TransactionRepository", "class_banking_app_repository2_1_1_repositorii_1_1_transaction_repository.html", null ]
      ] ]
    ] ],
    [ "BankingAppplication.Operationen", "class_banking_appplication_1_1_operationen.html", null ],
    [ "BankingAppDB.Models.Person", "class_banking_app_d_b_1_1_models_1_1_person.html", null ],
    [ "BankingAppDB.Models.PersonAccount", "class_banking_app_d_b_1_1_models_1_1_person_account.html", null ],
    [ "BankingAppplication.Program", "class_banking_appplication_1_1_program.html", null ],
    [ "Teszt.QueryTest", "class_teszt_1_1_query_test.html", null ],
    [ "Teszt.TestChangeData", "class_teszt_1_1_test_change_data.html", null ],
    [ "Teszt.TestNewAccount", "class_teszt_1_1_test_new_account.html", null ],
    [ "BankingAppDB.Models.Transaction", "class_banking_app_d_b_1_1_models_1_1_transaction.html", null ],
    [ "BankAppLogic.Models.TransactionSumInPeriod", "class_bank_app_logic_1_1_models_1_1_transaction_sum_in_period.html", null ]
];