var interface_bank_app_logic_1_1_i_data_query =
[
    [ "GetAllAccount", "interface_bank_app_logic_1_1_i_data_query.html#a43ebca481a84a8f4059aaf7a7884a847", null ],
    [ "GetAllPersons", "interface_bank_app_logic_1_1_i_data_query.html#a3684ace5dbc3c8bfaed4fa693bcc3a27", null ],
    [ "GetPersonToAccount", "interface_bank_app_logic_1_1_i_data_query.html#a12ae52241489769e2f7fcb99adddaadb", null ],
    [ "GetTransactions", "interface_bank_app_logic_1_1_i_data_query.html#a9c9994a667241b8e18dacdd762dce17a", null ],
    [ "SumofAPeriod", "interface_bank_app_logic_1_1_i_data_query.html#a6c4d4adb074be358ca0aaa35806e4c0d", null ],
    [ "TransactionForAnAccount", "interface_bank_app_logic_1_1_i_data_query.html#a6ff7a92316ddfde966e4ea0567533285", null ],
    [ "TransactionsOverTreshold", "interface_bank_app_logic_1_1_i_data_query.html#add185f1793e8ed058ef565f9a25d3cc4", null ]
];