var class_banking_app_d_b_1_1_models_1_1_account =
[
    [ "Account", "class_banking_app_d_b_1_1_models_1_1_account.html#adaa4900454f4989b3b9b589a7fb932ec", null ],
    [ "ToString", "class_banking_app_d_b_1_1_models_1_1_account.html#ac0b93ac397e5508078946a2c77a6954e", null ],
    [ "Id", "class_banking_app_d_b_1_1_models_1_1_account.html#a191d9c748c805a2999b0fc84901db937", null ],
    [ "IsClosed", "class_banking_app_d_b_1_1_models_1_1_account.html#ab20a9ef1fb4e0b4e447b1949f5b185eb", null ],
    [ "IsInactive", "class_banking_app_d_b_1_1_models_1_1_account.html#a4d4e32ec9b534f0a2bec2bdc1488fd89", null ],
    [ "Name", "class_banking_app_d_b_1_1_models_1_1_account.html#ac7a72fe4bea83e78252727a949bec579", null ],
    [ "Personaccount", "class_banking_app_d_b_1_1_models_1_1_account.html#a26b3f9bf5a9ab1e28822da4222420eb1", null ],
    [ "Transactions", "class_banking_app_d_b_1_1_models_1_1_account.html#a295d6ee21b4898790308482227bfa25c", null ],
    [ "Type", "class_banking_app_d_b_1_1_models_1_1_account.html#a0b4ed6944edcebff8a5f79eb4c8cfc68", null ]
];