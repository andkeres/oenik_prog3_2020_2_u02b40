var namespaces_dup =
[
    [ "Edit a file", "md__c___klon__r_e_a_d_m_e.html#autotoc_md1", null ],
    [ "Create a file", "md__c___klon__r_e_a_d_m_e.html#autotoc_md3", null ],
    [ "Clone a repository", "md__c___klon__r_e_a_d_m_e.html#autotoc_md5", null ],
    [ "BankAppLogic", "namespace_bank_app_logic.html", "namespace_bank_app_logic" ],
    [ "BankingAppDB", "namespace_banking_app_d_b.html", "namespace_banking_app_d_b" ],
    [ "BankingAppplication", "namespace_banking_appplication.html", null ],
    [ "BankingAppRepository2", "namespace_banking_app_repository2.html", "namespace_banking_app_repository2" ],
    [ "Teszt", "namespace_teszt.html", null ]
];