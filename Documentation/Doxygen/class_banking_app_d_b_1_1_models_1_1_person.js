var class_banking_app_d_b_1_1_models_1_1_person =
[
    [ "Person", "class_banking_app_d_b_1_1_models_1_1_person.html#a02bfdc554245053c35e7a0a039e962cd", null ],
    [ "ToString", "class_banking_app_d_b_1_1_models_1_1_person.html#aea09024230ae7e318222ba0e7e5e7644", null ],
    [ "AccountId", "class_banking_app_d_b_1_1_models_1_1_person.html#aa968c6a8b9b94d1e47f2bcc1e75667a7", null ],
    [ "Address", "class_banking_app_d_b_1_1_models_1_1_person.html#a29354bde54e7d5ee128cccc2392986bf", null ],
    [ "AnyjaNeve", "class_banking_app_d_b_1_1_models_1_1_person.html#af2c647eb90cf4674e6da35e929839718", null ],
    [ "Id", "class_banking_app_d_b_1_1_models_1_1_person.html#a6f658cc5f3e5d8caa2b3f9bc1cb5f6df", null ],
    [ "Név", "class_banking_app_d_b_1_1_models_1_1_person.html#a415a068b89426724776bca31718c1968", null ],
    [ "Personaccount", "class_banking_app_d_b_1_1_models_1_1_person.html#a5052fe78bb5fa8f893ef8171acb5528a", null ],
    [ "SZülidő", "class_banking_app_d_b_1_1_models_1_1_person.html#ab1d4582ed6f0d2088a542a3e369a1319", null ]
];