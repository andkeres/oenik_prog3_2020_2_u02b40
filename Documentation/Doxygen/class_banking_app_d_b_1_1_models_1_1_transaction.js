var class_banking_app_d_b_1_1_models_1_1_transaction =
[
    [ "ToString", "class_banking_app_d_b_1_1_models_1_1_transaction.html#ae4bc16d77a0528259ee182543545e9a2", null ],
    [ "AccountId", "class_banking_app_d_b_1_1_models_1_1_transaction.html#ace8f5f61fc8082a5dc6df0a9a0eefc81", null ],
    [ "Accounts", "class_banking_app_d_b_1_1_models_1_1_transaction.html#a1a6117022f3e6536f12eb97c0733ad71", null ],
    [ "Amount", "class_banking_app_d_b_1_1_models_1_1_transaction.html#a772550d7d56e33bae2a0f3f557b850a4", null ],
    [ "Currency", "class_banking_app_d_b_1_1_models_1_1_transaction.html#a9ffaf6870397fbb0d8f1b049fe51f28f", null ],
    [ "Id", "class_banking_app_d_b_1_1_models_1_1_transaction.html#a250dff9b5cd13da2acd3f16a7aee9616", null ],
    [ "TransferTime", "class_banking_app_d_b_1_1_models_1_1_transaction.html#a99b6e8ca13ca0840018add4d29474a35", null ],
    [ "Type", "class_banking_app_d_b_1_1_models_1_1_transaction.html#a37af98fa114f779bfbbb78608a2cfdad", null ]
];