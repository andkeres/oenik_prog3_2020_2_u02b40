var class_banking_app_repository2_1_1_repositorii_1_1_person_repository =
[
    [ "PersonRepository", "class_banking_app_repository2_1_1_repositorii_1_1_person_repository.html#aaa31964424afd3165a2cbc591ba02374", null ],
    [ "AddNew", "class_banking_app_repository2_1_1_repositorii_1_1_person_repository.html#acfca7e52b1a16d0d27cd68ceb23b723f", null ],
    [ "ChangeAddress", "class_banking_app_repository2_1_1_repositorii_1_1_person_repository.html#a7d8720e70f4ec442c58c8df3a41cd18e", null ],
    [ "ChangeName", "class_banking_app_repository2_1_1_repositorii_1_1_person_repository.html#ae1e5f99cdc26bc438f6c562669e0d9c9", null ],
    [ "GetByID", "class_banking_app_repository2_1_1_repositorii_1_1_person_repository.html#a7051fb5c5c232d15e96c778b161826e9", null ],
    [ "Update", "class_banking_app_repository2_1_1_repositorii_1_1_person_repository.html#a4bcfcd6a7f39d19a5765def5c8327cf7", null ]
];