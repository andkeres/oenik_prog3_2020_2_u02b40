var class_bank_app_logic_1_1_change_data =
[
    [ "ChangeData", "class_bank_app_logic_1_1_change_data.html#ac1a2e8f6d20d9ecd68c290c87bac1812", null ],
    [ "AccountClosed", "class_bank_app_logic_1_1_change_data.html#a0858953e16fa1c707bc8c02cf337db8b", null ],
    [ "AccountInactive", "class_bank_app_logic_1_1_change_data.html#aeff828b6b5b48672657cdbbea7df37a8", null ],
    [ "AssignPersonToAccount", "class_bank_app_logic_1_1_change_data.html#a76c99eb3b9bcb2d96f32cfc803dfb637", null ],
    [ "DeleteAccount", "class_bank_app_logic_1_1_change_data.html#abcb3ced73999d13d489b613b9ce669c0", null ],
    [ "DeletePerson", "class_bank_app_logic_1_1_change_data.html#a17da6d7ee372412dd0758854ee59e8e4", null ],
    [ "DesignPersonToAccount", "class_bank_app_logic_1_1_change_data.html#ab400eb8866803c5aa753247160412f30", null ],
    [ "ModifyAccountName", "class_bank_app_logic_1_1_change_data.html#a5393aefc1afb3b87aec279095c28295b", null ],
    [ "ModifyPersonAddress", "class_bank_app_logic_1_1_change_data.html#a72a05b4e9cbd92e022d00f69df695e9b", null ],
    [ "ModifyPersonName", "class_bank_app_logic_1_1_change_data.html#ab54a20417d9852121ef8ca2e4d919f4f", null ],
    [ "ModifyTransactionAmount", "class_bank_app_logic_1_1_change_data.html#ad08e540d642714e7b61f184094fae362", null ]
];