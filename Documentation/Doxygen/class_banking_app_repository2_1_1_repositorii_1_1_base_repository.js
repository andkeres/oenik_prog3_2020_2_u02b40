var class_banking_app_repository2_1_1_repositorii_1_1_base_repository =
[
    [ "BaseRepository", "class_banking_app_repository2_1_1_repositorii_1_1_base_repository.html#a2b21ea81d5f8dc354213ea1279b771ce", null ],
    [ "Commit", "class_banking_app_repository2_1_1_repositorii_1_1_base_repository.html#a63612ac173063f5258aa2066ee50e8e5", null ],
    [ "Delete", "class_banking_app_repository2_1_1_repositorii_1_1_base_repository.html#a7cdf266228f843bd99fb4bab2e154692", null ],
    [ "Find", "class_banking_app_repository2_1_1_repositorii_1_1_base_repository.html#af76c876001b9e39f768d33298f272651", null ],
    [ "GetAll", "class_banking_app_repository2_1_1_repositorii_1_1_base_repository.html#afd7db6578003bc55fcbad008b5c5290e", null ],
    [ "GetByID", "class_banking_app_repository2_1_1_repositorii_1_1_base_repository.html#a99073c7915be07e254c1021110bc076b", null ],
    [ "Update", "class_banking_app_repository2_1_1_repositorii_1_1_base_repository.html#afb4ed2def68f82b0828f38b8b5282d75", null ],
    [ "DbContext", "class_banking_app_repository2_1_1_repositorii_1_1_base_repository.html#aab3a0a0d7b930890600344f882e4bcab", null ]
];