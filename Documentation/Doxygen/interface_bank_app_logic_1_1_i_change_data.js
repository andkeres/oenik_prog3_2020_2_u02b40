var interface_bank_app_logic_1_1_i_change_data =
[
    [ "AccountClosed", "interface_bank_app_logic_1_1_i_change_data.html#aaeaed7a4ee0b5f6dde210d318f0e5800", null ],
    [ "AccountInactive", "interface_bank_app_logic_1_1_i_change_data.html#a600a694d30b12b9194c4e1dd958c5209", null ],
    [ "AssignPersonToAccount", "interface_bank_app_logic_1_1_i_change_data.html#ac4898e4cf81a333f17262322935a07bb", null ],
    [ "DeleteAccount", "interface_bank_app_logic_1_1_i_change_data.html#a869575330ea5bbe2494cc95ba7a98fee", null ],
    [ "DeletePerson", "interface_bank_app_logic_1_1_i_change_data.html#a9a7ecf7bca9ea922d5a54901a2a16d30", null ],
    [ "DesignPersonToAccount", "interface_bank_app_logic_1_1_i_change_data.html#a7a043293ffece9e1fd4b18a631a4eebd", null ],
    [ "ModifyAccountName", "interface_bank_app_logic_1_1_i_change_data.html#afb622d810b22409f17fbb7f7ea9f8562", null ],
    [ "ModifyPersonAddress", "interface_bank_app_logic_1_1_i_change_data.html#ae35491ed6c893683d76ee0af29d849de", null ],
    [ "ModifyPersonName", "interface_bank_app_logic_1_1_i_change_data.html#a29369a8c635e8860edc9540349255f66", null ],
    [ "ModifyTransactionAmount", "interface_bank_app_logic_1_1_i_change_data.html#a4d73e947c8af58481cad96335eb774aa", null ]
];