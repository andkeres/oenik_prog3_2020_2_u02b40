﻿// <copyright file="IPersonRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankingAppRepository2.Repositorii
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using BankingAppDB.Models;

    /// <summary>
    /// Repository for the person.
    /// </summary>
    public interface IPersonRepository : IRepository<Person>
    {
        /// <summary>
        /// Chage person Address.
        /// </summary>
        /// <param name="id">Person_id.</param>
        /// <param name="address">Address.</param>
        public void ChangeAddress(string id, string address);

        /// <summary>
        /// Change person name.
        /// </summary>
        /// <param name="id">Person_id.</param>
        /// <param name="name">Person_name.</param>
        public void ChangeName(string id, string name);

        /// <summary>
        /// Add new person.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="name">name.</param>
        /// <param name="szulido">BateofBirth.</param>
        /// <param name="anyjaNeve">MotherName.</param>
        /// <param name="address">Address.</param>
        public void AddNew(string id, string name, DateTime szulido, string anyjaNeve, string address);
    }
}
