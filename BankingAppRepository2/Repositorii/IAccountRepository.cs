﻿// <copyright file="IAccountRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankingAppRepository2.Repositorii
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using BankingAppDB.Models;
    using Microsoft.EntityFrameworkCore.Update.Internal;

    /// <summary>
    /// Account Repository.
    /// </summary>
    public interface IAccountRepository : IRepository<Account>
    {
        /// <summary>
        /// Add new account to repo.
        /// </summary>
        /// <param name="personId">Person_id.</param>
        /// <param name="accountId">Account_id.</param>
        /// <param name="type">Type.</param>
        public void AddNew(string personId, string accountId, string type);
        /*new IQueryable<PersonAccount> GetAll();

        new Account GetByID(int id);

        new IEnumerable<Account> Find(Func<Account, bool> filter);

        new void Update(Account entity);

        new void Delete(Account del);*/
    }
}
