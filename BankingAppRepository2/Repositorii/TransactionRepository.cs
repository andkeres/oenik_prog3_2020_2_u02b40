﻿// <copyright file="TransactionRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankingAppRepository2.Repositorii
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using BankingAppDB.Models;
    using BankingAppRepository2.Repositorii;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Transaction.
    /// </summary>
    public class TransactionRepository : BaseRepository<Transaction>, ITransactionRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionRepository"/> class.
        /// Constructor.
        /// </summary>
        /// <param name="dbContext">dbcontext.</param>
        public TransactionRepository(DbContext dbContext)
            : base(dbContext)
        {
        }

        /// <summary>
        /// Add new Transaction.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <param name="type">Type.</param>
        /// <param name="transfertime">Transfertime.</param>
        /// <param name="amount">Amount.</param>
        /// <param name="currency">Currency.</param>
        /// <param name="accountId">AccountId.</param>
        public void AddNew(string id, string type, DateTime transfertime, decimal amount, string currency, string accountId)
        {
            Transaction transaction = new Transaction();

            transaction.Id = id;
            transaction.Type = type;
            transaction.TransferTime = transfertime;
            transaction.Amount = amount;
            transaction.Currency = currency;
            transaction.AccountId = accountId;

            this.DbContext.Add<Transaction>(transaction);
            this.DbContext.SaveChanges();
        }

        /// <summary>
        /// Select for transaction by Id.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <returns>Transaction.</returns>
        public override Transaction GetByID(string id)
        {
            return this.GetAll().Single(trans => trans.Id == id);
        }

        /// <summary>
        /// Update the transaction.
        /// </summary>
        /// <param name="entity">Entity.</param>
        public override void Update(Transaction entity)
        {
           this.DbContext.Update<Transaction>(entity);
        }
    }
}
