﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankingAppRepository2.Repositorii
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Main interface for the repository.
    /// </summary>
    /// <typeparam name="T">T.</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// GetAll.
        /// </summary>
        /// <returns>IQueryable.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// GetById.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>T.</returns>
        T GetByID(string id);

        /// <summary>
        /// Find.
        /// </summary>
        /// <param name="filter">filter.</param>
        /// <returns>IEnumerable.</returns>
        IEnumerable<T> Find(Func<T, bool> filter);

        /// <summary>
        /// Update.
        /// </summary>
        /// <param name="entity">Entity.</param>
        void Update(T entity);

        /// <summary>
        /// Delete.
        /// </summary>
        /// <param name="del">del.</param>
        void Delete(T del);
    }
}
