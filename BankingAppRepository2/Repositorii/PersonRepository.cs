﻿// <copyright file="PersonRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankingAppRepository2.Repositorii
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using BankingAppDB.Models;
    using BankingAppRepository2.Repositorii;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// PersonRepository.
    /// </summary>
    public class PersonRepository : BaseRepository<Person>, IPersonRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PersonRepository"/> class.
        /// Constructor.
        /// </summary>
        /// <param name="dbContext">dbcontext.</param>
        public PersonRepository(DbContext dbContext)
            : base(dbContext)
        {
        }

        /// <summary>
        /// Add new person.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="name">name.</param>
        /// <param name="szulido">DateofBirth.</param>
        /// <param name="anyjaNeve">MotherName.</param>
        /// <param name="address">Address.</param>
        public void AddNew(string id, string name, DateTime szulido, string anyjaNeve, string address)
        {
            Person person = new Person();
            person.Id = id;
            person.Név = name;
            person.SZülidő = szulido;
            person.AnyjaNeve = anyjaNeve;
            person.Address = address;

            this.DbContext.Add<Person>(person);
            this.DbContext.SaveChanges();
        }

        /// <summary>
        /// Change address of a person.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="address">address.</param>
        public void ChangeAddress(string id, string address)
        {
            this.GetAll().Single(person => person.Id == id).Address = address;
            this.DbContext.SaveChanges();
        }

        /// <summary>
        /// Change name of a person.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="name">name.</param>
        public void ChangeName(string id, string name)
        {
            this.GetAll().Single(person => person.Id == id).Név = name;

            this.DbContext.SaveChanges();
        }

        /// <summary>
        /// Select by the Id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>Person.</returns>
        public override Person GetByID(string id)
        {
            return this.GetAll().SingleOrDefault(person => person.Id == id);
        }

        /// <summary>
        /// Update of the person.
        /// </summary>
        /// <param name="entity">Entity.</param>
        public override void Update(Person entity)
        {
            this.DbContext.Update<Person>(entity);
        }
    }
}
