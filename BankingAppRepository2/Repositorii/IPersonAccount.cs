﻿// <copyright file="IPersonAccount.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankingAppRepository2.Repositorii
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using BankingAppDB.Models;

    /// <summary>
    /// Interface for person-account relation.
    /// </summary>
    public interface IPersonAccount : IRepository<PersonAccount>
    {
        /// <summary>
        /// Add ne relation.
        /// </summary>
        /// <param name="id">Person_id.</param>
        /// <param name="id2">Account_id.</param>
        public void AddNewRelation(string id, string id2);

        /// <summary>
        /// Delete relation.
        /// </summary>
        /// <param name="id">Person_id.</param>
        /// <param name="id2">Account_id.</param>
        public void DeleteRelation(string id, string id2);

        // *public void DeleteRelation(string id, string id2);
    }
}
