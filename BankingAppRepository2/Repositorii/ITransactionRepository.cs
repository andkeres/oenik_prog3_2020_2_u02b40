﻿// <copyright file="ITransactionRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankingAppRepository2.Repositorii
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using BankingAppDB.Models;

    /// <summary>
    /// Transaction Repository.
    /// </summary>
    public interface ITransactionRepository : IRepository<Transaction>
    {
        /// <summary>
        /// Add new Transaction to repo.
        /// </summary>
        /// <param name="id">Id.</param>
        /// <param name="type">Type.</param>
        /// <param name="transfertime">TimeofTransaction.</param>
        /// <param name="amount">Amount.</param>
        /// <param name="currency">Currency.</param>
        /// <param name="accountId">AccountId.</param>
        public void AddNew(string id, string type, DateTime transfertime, decimal amount, string currency, string accountId);
    }
}
