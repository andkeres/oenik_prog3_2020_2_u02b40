﻿// <copyright file="PersonAccountRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankingAppRepository2.Repositorii
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using BankingAppDB.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Repository for person-account relationships.
    /// </summary>
    public class PersonAccountRepository : BaseRepository<PersonAccount>, IPersonAccount
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PersonAccountRepository"/> class.
        /// PersonAccount DBContext.
        /// </summary>
        /// <param name="dbContext">dbContext.</param>
        public PersonAccountRepository(DbContext dbContext)
            : base(dbContext)
        {
        }

        /// <summary>
        /// Add new relation.
        /// </summary>
        /// <param name="id">person_id.</param>
        /// <param name="id2">account_id.</param>
        public void AddNewRelation(string id, string id2)
        {
            PersonAccount pa = new PersonAccount();

            pa.AccountId = id;
            pa.PersonId = id2;
            this.DbContext.Add<PersonAccount>(pa);
            this.DbContext.SaveChanges();
        }

        /// <summary>
        /// Delete relation.
        /// </summary>
        /// <param name="id">person_id.</param>
        /// <param name="id2">account_id.</param>
        public void DeleteRelation(string id, string id2)
        {
        }

        /// <summary>
        /// Search for relation by id.
        /// </summary>
        /// <param name="id">relation_id.</param>
        /// <returns>PersonAccount_rel.</returns>
        public override PersonAccount GetByID(string id)
        {
            return this.GetAll().Single(pa => pa.PersonId == id);
        }

        /// <summary>
        /// Update.
        /// </summary>
        /// <param name="entity">PersonAccount.</param>
        public override void Update(PersonAccount entity)
        {
            this.DbContext.Update<PersonAccount>(entity);
        }
    }
}
