﻿// <copyright file="AccountRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankingAppRepository2.Repositorii
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using BankingAppDB.Models;
    using BankingAppRepository2.Repositorii;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Account repository.
    /// </summary>
    public class AccountRepository : BaseRepository<Account>, IAccountRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AccountRepository"/> class.
        /// Constructor.
        /// </summary>
        /// <param name="dbContext">dbContext.</param>
        public AccountRepository(DbContext dbContext)
            : base(dbContext)
        {
        }

        /// <summary>
        /// Add new account to repo.
        /// </summary>
        /// <param name="id">Account_id.</param>
        /// <param name="nev">Account_name.</param>
        /// <param name="type">Account_type.</param>
        public void AddNew(string id, string nev, string type)
        {
            Account account = new Account();
            account.Id = id;
            account.Name = nev;
            account.Type = type;
            account.IsClosed = false;
            account.IsInactive = false;
            this.DbContext.Add<Account>(account);
            this.DbContext.SaveChanges();
        }

        /// <summary>
        /// Select for the Id.
        /// </summary>
        /// <param name="id">Account_id.</param>
        /// <returns>Account.</returns>
        public override Account GetByID(string id)
        {
            return this.GetAll().Single(account => account.Id == id);
        }

        /// <summary>
        /// Update the account.
        /// </summary>
        /// <param name="entity">Account.</param>
        public override void Update(Account entity)
        {
            this.DbContext.Update<Account>(entity);
        }
    }
}
