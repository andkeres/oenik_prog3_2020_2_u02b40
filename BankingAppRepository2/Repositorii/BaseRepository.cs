﻿// <copyright file="BaseRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankingAppRepository2.Repositorii
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using BankingAppDB.Models;
    using BankingAppRepository2.Repositorii;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// BaseRepository generic implementation.
    /// </summary>
    /// <typeparam name="T">T_class.</typeparam>
    ///
    public abstract class BaseRepository<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// DBContext.
        /// </summary>
        internal readonly DbContext DbContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseRepository{T}"/> class.
        /// Constructor.
        /// </summary>
        /// <param name="dbContext">dbcontext.</param>
        public BaseRepository(DbContext dbContext)
        {
            this.DbContext = dbContext;
        }

        /// <summary>
        /// Save the changes.
        /// </summary>
        public void Commit()
        {
            this.DbContext.SaveChanges();
        }

        /// <summary>
        /// Deletion.
        /// </summary>
        /// <param name="delete">delete.</param>
        public void Delete(T delete)
        {
            this.DbContext.Remove(delete);
            this.DbContext.SaveChanges();
        }

        /// <summary>
        /// Search for entity.
        /// </summary>
        /// <param name="filter">search.</param>
        /// <returns>Entity.</returns>
        public IEnumerable<T> Find(Func<T, bool> filter)
        {
            return this.GetAll().Where(filter);
        }

        /// <summary>
        /// Get all entities.
        /// </summary>
        /// <returns>IEnumerable.</returns>
        public IQueryable<T> GetAll()
        {
            return this.DbContext.Set<T>();
        }

        /// <summary>
        /// Select for entity.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>IQuryable.</returns>
        public abstract T GetByID(string id);

        /// <summary>
        /// Update entity.
        /// </summary>
        /// <param name="entity">Entity.</param>
        public abstract void Update(T entity);
    }
}
