﻿// <copyright file="NewTransaction.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankAppLogic
{
    using System;
    using BankingAppRepository2.Repositorii;

    /// <summary>
    /// Class for Logic fuction New Transaction.
    /// </summary>
    public class NewTransaction : INewTransaction
    {
        private readonly ITransactionRepository transactionRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="NewTransaction"/> class.
        /// Create new Transaction.
        /// </summary>
        /// <param name="transactionRepository">Transactiorepository.</param>
        public NewTransaction(ITransactionRepository transactionRepository)
        {
            this.transactionRepository = transactionRepository;
        }

        /// <summary>
        /// Add new Transaction.
        /// </summary>
        /// <param name="id">transaction_id.</param>
        /// <param name="type">transaction_type.</param>
        /// <param name="transfertime">transaction_date.</param>
        /// <param name="amount">transaction_amount.</param>
        /// <param name="currency">transaction_currency.</param>
        /// <param name="accountId">account_id.</param>
        public void CreateNewTransaction(string id, string type, DateTime transfertime, decimal amount, string currency, string accountId)
        {
            this.transactionRepository.AddNew(id, type, transfertime, amount, currency, accountId);
        }

        /// <summary>
        /// deletion of transaction.
        /// </summary>
        /// <param name="id">transaction_id.</param>
        public void DeleteTransaction(string id)
        {
            this.transactionRepository.Delete(this.transactionRepository.GetByID(id));
        }
    }
}
