﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1051:Do not declare visible instance fields", Justification = "<Pending>", Scope = "member", Target = "~F:BankAppLogic.Models.TransactionSumInPeriod.amount")]
[assembly: SuppressMessage("Design", "CA1051:Do not declare visible instance fields", Justification = "<Pending>", Scope = "member", Target = "~F:BankAppLogic.Models.TransactionSumInPeriod.accountId")]
[assembly: SuppressMessage("StyleCop.CSharp.NamingRules", "SA1307:Accessible fields should begin with upper-case letter", Justification = "<Pending>", Scope = "member", Target = "~F:BankAppLogic.Models.TransactionSumInPeriod.accountId")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1404:Code analysis suppression should have justification", Justification = "<Pending>")]
[assembly: SuppressMessage("Design", "CA1051:Do not declare visible instance fields", Justification = "<Pending>", Scope = "member", Target = "~F:BankAppLogic.Models.TransactionSumInPeriod.AccountId")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:Fields should be private", Justification = "<Pending>", Scope = "member", Target = "~F:BankAppLogic.Models.TransactionSumInPeriod.AccountId")]
[assembly: SuppressMessage("Design", "CA1051:Do not declare visible instance fields", Justification = "<Pending>", Scope = "member", Target = "~F:BankAppLogic.Models.TransactionSumInPeriod.Amount")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:Fields should be private", Justification = "<Pending>", Scope = "member", Target = "~F:BankAppLogic.Models.TransactionSumInPeriod.Amount")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "<Pending>", Scope = "member", Target = "~M:BankAppLogic.DataQuery.#ctor(BankingAppRepository2.Repositorii.IRepository{BankingAppDB.Models.Person},BankingAppRepository2.Repositorii.IRepository{BankingAppDB.Models.Account},BankingAppRepository2.Repositorii.IRepository{BankingAppDB.Models.Transaction},BankingAppRepository2.Repositorii.IRepository{BankingAppDB.Models.PersonAccount})")]
