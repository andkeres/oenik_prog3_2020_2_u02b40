﻿// <copyright file="DataQuery.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankAppLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using BankAppLogic.Models;
    using BankingAppDB.Models;
    using BankingAppRepository2.Repositorii;

    /// <summary>
    ///  Class for data query logic.
    /// </summary>
    public class DataQuery : IDataQuery
    {
        private readonly IRepository<Person> personrepository;
        private readonly IRepository<Account> accountrepository;
        private readonly IRepository<Transaction> transactionrepository;
        private readonly IRepository<PersonAccount> personaccountrepository;

        /// <summary>
        /// Summary.
        /// </summary>
        /// <param name="threshold">treshold limit.</param>
        /// <returns>generic.</returns>
        public Task<IList<AccountToTransaction>> TransactionsOverTresholdAsync(object threshold)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataQuery"/> class.
        /// Constructor.
        /// </summary>
        /// <param name="personrepository">Personrepository.</param>
        /// <param name="accountrepository">Accountrepository.</param>
        /// <param name="transactionrepository">Tranaósactionrepository.</param>
        /// <param name="personaccountrepository">Personaccountrepository.</param>
        public DataQuery(IRepository<Person> personrepository, IRepository<Account> accountrepository, IRepository<Transaction> transactionrepository, IRepository<PersonAccount> personaccountrepository)
        {
            this.accountrepository = accountrepository;
            this.transactionrepository = transactionrepository;
            this.personrepository = personrepository;
            this.personaccountrepository = personaccountrepository;
        }

        /// <summary>
        /// Get al accounts.
        /// </summary>
        /// <returns>Collectio of accounts.</returns>
        public ICollection<Account> GetAllAccount()
        {
            return this.accountrepository.GetAll()?.ToList() ?? new List<Account>();
        }

        /// <summary>
        /// get all persons.
        /// </summary>
        /// <returns>Collection of persons.</returns>
        public ICollection<Person> GetAllPersons()
        {
            return this.personrepository.GetAll()?.ToList() ?? new List<Person>();
        }

        /// <summary>
        /// Get all Transactions.
        /// </summary>
        /// <returns>Collection of transactions.</returns>
        public ICollection<Transaction> GetTransactions()
        {
            return this.transactionrepository.GetAll()?.ToList() ?? new List<Transaction>();
        }

        /// <summary>
        /// Get all Person-Account relations.
        /// </summary>
        /// <returns>PersonAccount collections.</returns>
        public ICollection<PersonAccount> GetPersonToAccount()
        {
            return this.personaccountrepository.GetAll()?.ToList() ?? new List<PersonAccount>();
        }

        /// <summary>
        /// List of account - transaction relations.
        /// </summary>
        /// <returns>Account - Transaction collection.</returns>
        public ICollection<AccountToTransaction> TransactionAccountRelations()
        {
            var q1 = from account in this.accountrepository.GetAll()
                     from transaction in account.Transactions
                     where account.Transactions != null
                     select new { accountid = account.Id, transactionid = transaction.Id };

            return (ICollection<AccountToTransaction>)q1;
        }

        /// <summary>
        /// List of transactions for an account.
        /// </summary>
        /// <param name="id">account_id.</param>
        /// <returns>Transaction_collection.</returns>
        public ICollection<Transaction> TransactionForAnAccount(string id)
        {
            var q1 = from transaction in this.transactionrepository.GetAll()
                     where transaction.AccountId == id
                     select transaction;

            return q1.ToList();
        }

        /// <summary>
        /// Transactios over a treshold limit.
        /// </summary>
        /// <param name="treshold">treshold_limit.</param>
        /// <returns>transaction.</returns>
        public IList<AccountToTransaction> TransactionsOverTreshold(decimal treshold)
        {
            var q1 =

                     from transaction in this.transactionrepository.GetAll()
                     where transaction.Amount > treshold
                     join account2 in this.accountrepository.GetAll()
                     on transaction.AccountId equals account2.Id
                    select new AccountToTransaction { Accountid = account2.Id, Transactionid = transaction.Id, Amount = transaction.Amount, Accounttype = account2.Type };

                    /* let item = new { AccountId = account.Id, TransactionData = transaction.Id, Amount = transaction.Amount }
                     group item by account.Id into grp
                     select grp;*/

            return q1.ToList();
        }

        /// <summary>
        /// Sum of the transactions in a period.
        /// </summary>
        /// <returns>Account_transaction.</returns>
        public IList<TransactionSumInPeriod> SumofAPeriod()
        {
            var q1 = from transaction in this.transactionrepository.GetAll()
                group transaction by transaction.AccountId into grp
                select new TransactionSumInPeriod()
                {
                    AccountId = grp.Key,
                    Amount = grp.Sum(x => x.Amount),
                };

            return q1.ToList();
        }

        /// <summary>
        /// Async method for PersonToAccount.
        /// </summary>
        /// <returns>Task collection.</returns>
        public Task<ICollection<PersonAccount>> GetPersonToAccountAsync()
        {
            return Task.Run(() => { return this.GetPersonToAccount(); });
        }

        /// <summary>
        /// Async method for TransactionForAnAccount.
        /// </summary>
        /// <param name="accountid">AccountId.</param>
        /// <returns>Task collection.</returns>
        public Task<ICollection<Transaction>> TransactionForAnAccountAsync(string accountid)
        {
            return Task.Run(() => { return this.TransactionForAnAccount(accountid); });
        }

        /// <summary>
        /// Async method for TransactionsOverTreshold.
        /// </summary>
        /// <param name="treshold">treshold.</param>
        /// <returns>Task collection.</returns>
        public Task<IList<AccountToTransaction>> TransactionsOverTresholdAsync(decimal treshold)
        {
            return Task.Run(() => { return this.TransactionsOverTreshold(treshold); });
        }

        /// <summary>
        /// Async method for SumofAPeriod.
        /// </summary>
        /// <returns>Task collection.</returns>
        public Task<IList<TransactionSumInPeriod>> SumofAPeriodAsync()
        {
            return Task.Run(() => { return this.SumofAPeriod(); });
        }
    }
    }
