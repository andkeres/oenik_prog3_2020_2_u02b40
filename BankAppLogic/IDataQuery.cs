﻿// <copyright file="IDataQuery.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankAppLogic
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using BankAppLogic.Models;
    using BankingAppDB.Models;

    /// <summary>
    /// Interface for data querries.
    /// </summary>
    public interface IDataQuery
    {
        /// <summary>
        /// List all persons.
        /// </summary>
        /// <returns>Person_list.</returns>
        ICollection<Person> GetAllPersons();

        /// <summary>
        /// List of all accounts.
        /// </summary>
        /// <returns>Account_list.</returns>
        ICollection<Account> GetAllAccount();

        /// <summary>
        /// List of all transaction.
        /// </summary>
        /// <returns>Transaction_list.</returns>
        ICollection<Transaction> GetTransactions();

        /// <summary>
        /// List of all Person-Account relations.
        /// </summary>
        /// <returns>Person-Account_list.</returns>
        ICollection<PersonAccount> GetPersonToAccount();

        /// <summary>
        /// Async method: List of all Person-Account relations.
        /// </summary>
        /// <returns>Task collection.</returns>
        Task<ICollection<PersonAccount>> GetPersonToAccountAsync();

        /// <summary>
        /// List of all transactions.
        /// </summary>
        /// <param name="id">transaction_id.</param>
        /// <returns>Transaction_list.</returns>
        ICollection<Transaction> TransactionForAnAccount(string id);

        /// <summary>
        /// Async method: List of all Transaction-Account relations.
        /// </summary>
        /// <param name="id">account_id.</param>
        /// <returns>Task collection.</returns>
        Task<ICollection<Transaction>> TransactionForAnAccountAsync(string id);

        /// <summary>
        /// List of all Transactions over treshold.
        /// </summary>
        /// <param name="treshold">treshold_value.</param>
        /// <returns>Account-Transaction_list.</returns>
        IList<AccountToTransaction> TransactionsOverTreshold(decimal treshold);

        /// <summary>
        /// List of all Transactions over treshold.
        /// </summary>
        /// <param name="treshold">treshold_value.</param>
        /// <returns>Account-Transaction_list.</returns>
        Task<IList<AccountToTransaction>> TransactionsOverTresholdAsync(decimal treshold);

        /// <summary>
        /// Sum of transaction amount in a period.
        /// </summary>
        /// <returns>Transaction_sum.</returns>
        IList<TransactionSumInPeriod> SumofAPeriod();

        /// <summary>
        /// Async method: List of all Transactions over Tresshold.
        /// </summary>
        /// <returns>Task collection.</returns>
        Task<IList<TransactionSumInPeriod>> SumofAPeriodAsync();
    }
}
