﻿// <copyright file="NewAccount.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankAppLogic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using BankingAppRepository2.Repositorii;

/// <summary>
/// Logic function new account.
/// </summary>
    public class NewAccount : INewAccount
    {
        private readonly IAccountRepository accountrepository;
        private readonly IPersonRepository personRepository;
        private IPersonAccount personaccountrepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="NewAccount"/> class.
        /// Constructor.
        /// </summary>
        /// <param name="accountrepository">Accountrepository.</param>
        /// <param name="personrepository">Personrepository.</param>
        /// <param name="personaccountrepository">PersonAccountrepository.</param>
        public NewAccount(IAccountRepository accountrepository, IPersonRepository personrepository, IPersonAccount personaccountrepository)
        {
            this.accountrepository = accountrepository;
            this.personRepository = personrepository;
            this.personaccountrepository = personaccountrepository;
        }

        /*public void AssignPersonToAccount(string id1, string id2)
        {
            personaccountrepository.A
        }*/

        /// <summary>
        /// Create new account.
        /// </summary>
        /// <param name="id">account_id.</param>
        /// <param name="name">account_name.</param>
        /// <param name="type">account_type.</param>
        public void CreateNewAccount(string id, string name, string type)
        {
            this.accountrepository.AddNew(id, name, type);
        }

        /// <summary>
        /// Create new person.
        /// </summary>
        /// <param name="id">person_id.</param>
        /// <param name="name">person_name.</param>
        /// <param name="anyjaneve">mother_name.</param>
        /// <param name="address">address.</param>
        /// <param name="dob">dateOfBirth.</param>
        public void CreateNewPerson(string id, string name, string anyjaneve, string address, DateTime dob)
        {
            this.personRepository.AddNew(id, name, dob, anyjaneve, address);
        }
    }
}
