﻿// <copyright file="AccountToTransaction.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankAppLogic.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Assign Account to Transaction.
    /// </summary>
    public class AccountToTransaction
    {
  /// <summary>
  /// Gets or sets transaction Id.
  /// </summary>
         public string Transactionid { get; set; }

        /// <summary>
        /// Gets or sets accountid.
        /// </summary>
         public string Accountid { get; set; }

        /// <summary>
        /// Gets or sets account type.
        /// </summary>
         public string Accounttype { get; set; }

        /// <summary>
        /// Gets or sets account amount.
        /// </summary>
         public decimal Amount { get; set; }

        /// <summary>
        ///  To string methosód.
        /// </summary>
        /// <returns>ToString.</returns>
         public override string ToString()
        {
            return "AccountId: " + this.Accountid + "AccountType: " + this.Accounttype + "Amount: " + this.Amount;
        }
    }
}
