﻿// <copyright file="TransactionSumInPeriod.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankAppLogic.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Sum of transactions in a period - specific class.
    /// </summary>
    public class TransactionSumInPeriod
    {
        /// <summary>
        /// Accountid property.
        /// </summary>
        public string AccountId;

        /// <summary>
        /// Amount property.
        /// </summary>
        public decimal Amount;

        /// <summary>
        /// To String method.
        /// </summary>
        /// <returns>Amount.</returns>
        public override string ToString()
        {
            return "AccountId: " + this.AccountId + " Amount: " + this.Amount;
        }
    }
}
