﻿// <copyright file="INewAccount.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankAppLogic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Interface for new transaction.
    /// </summary>
    public interface INewAccount
    {
        /// <summary>
        /// Create new account.
        /// </summary>
        /// <param name="id">account_id.</param>
        /// <param name="name">account_name.</param>
        /// <param name="type">account_tyye.</param>
         void CreateNewAccount(string id, string name, string type);

        /// <summary>
        /// Create new person.
        /// </summary>
        /// <param name="id">person_id.</param>
        /// <param name="name">person_name.</param>
        /// <param name="anyjaneve">mother_name.</param>
        /// <param name="address">address.</param>
        /// <param name="dob">date_of_birth.</param>
         void CreateNewPerson(string id, string name, string anyjaneve, string address, DateTime dob);

       // * void AssignPersonToAccount(string id, string id2);
    }
}
