﻿// <copyright file="ChangeData.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankAppLogic
{
    using BankingAppRepository2.Repositorii;

    /// <summary>
    /// ChangeData logic function.
    /// </summary>
    public class ChangeData : IChangeData
    {
        private readonly IPersonRepository personRepository;
        private readonly IAccountRepository accountRepository;
        private readonly ITransactionRepository transactionRepository;
        private readonly IPersonAccount personaccountRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChangeData"/> class.
        /// Constructor.
        /// </summary>
        /// <param name="personrepository">personrepository.</param>
        /// <param name="accountrepository">accountrepository.</param>
        /// <param name="transactionrepositpry">transactionrepository.</param>
        /// <param name="personaccountrepository">personaccountrepository.</param>
        public ChangeData(IPersonRepository personrepository, IAccountRepository accountrepository, ITransactionRepository transactionrepositpry, IPersonAccount personaccountrepository)
        {
            this.accountRepository = accountrepository;
            this.personRepository = personrepository;
            this.transactionRepository = transactionrepositpry;
            this.personaccountRepository = personaccountrepository;
}

        /// <summary>
        /// Modify Person name.
        /// </summary>
        /// <param name="id">person_id.</param>
        /// <param name="name">PersonName.</param>
        public void ModifyPersonName(string id, string name)
        {
            this.personRepository.ChangeName(id, name);
        }

        /// <summary>
        /// Delete Person.
        /// </summary>
        /// <param name="id">person_id.</param>
        public void DeletePerson(string id)
        {
            this.personRepository.Delete(this.personRepository.GetByID(id));
        }

        /// <summary>
        /// Delete Account.
        /// </summary>
        /// <param name="id">account_id.</param>
        public void DeleteAccount(string id)
        {
            this.accountRepository.Delete(this.accountRepository.GetByID(id));
        }

        /// <summary>
        /// Modify Account Name.
        /// </summary>
        /// <param name="id">accont_id.</param>
        /// <param name="newname">new_name.</param>
        public void ModifyAccountName(string id, string newname)
        {
            this.accountRepository.GetByID(id).Name = newname;
        }

        /// <summary>
        /// ModifyTransaction Amount.
        /// </summary>
        /// <param name="id">trans_id.</param>
        /// <param name="amount">trand_amount.</param>
        public void ModifyTransactionAmount(string id, decimal amount)
        {
            this.transactionRepository.GetByID(id).Amount = amount;
        }

        /// <summary>
        /// Inactivate account.
        /// </summary>
        /// <param name="id">account_id.</param>
        public void AccountInactive(string id)
        {
            this.accountRepository.GetByID(id).IsInactive = true;
        }

        /// <summary>
        /// Close account.
        /// </summary>
        /// <param name="id">account_id.</param>
        public void AccountClosed(string id)
        {
            this.accountRepository.GetByID(id).IsClosed = true;
        }

        /// <summary>
        /// Modify person Address.
        /// </summary>
        /// <param name="id">person_id.</param>
        /// <param name="address">person_address.</param>
        public void ModifyPersonAddress(string id, string address)
        {
            this.personRepository.GetByID(id).Address = address;
        }

        /// <summary>
        /// Assign Person to Account.
        /// </summary>
        /// <param name="personid">person_id.</param>
        /// <param name="accountid">account_id.</param>
        public void AssignPersonToAccount(string personid, string accountid)
        {
            this.personaccountRepository.AddNewRelation(personid, accountid);
        }

        /// <summary>
        /// Remove assignment.
        /// </summary>
        /// <param name="personid">person_id.</param>
        /// <param name="accountid">account_id.</param>
        public void DesignPersonToAccount(string personid, string accountid)
        {
            this.personaccountRepository.DeleteRelation(personid, accountid);
        }
    }
}
