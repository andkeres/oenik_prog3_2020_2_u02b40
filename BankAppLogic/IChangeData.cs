﻿// <copyright file="IChangeData.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankAppLogic
{
    /// <summary>
    /// Interface for logic function ChangeData.
    /// </summary>
    public interface IChangeData
    {
        /// <summary>
        /// Name modification.
        /// </summary>
        /// <param name="id">person_id.</param>
        /// <param name="name">person_name.</param>
        void ModifyPersonName(string id, string name);

        /// <summary>
        /// Address modification.
        /// </summary>
        /// <param name="id">person_id.</param>
        /// <param name="address">person_address.</param>
        void ModifyPersonAddress(string id, string address);

        /// <summary>
        /// Account Name  modification.
        /// </summary>
        /// <param name="id">person_id.</param>
        /// <param name="newname">person_name.</param>
        void ModifyAccountName(string id, string newname);

        /// <summary>
        /// Inactivate Account.
        /// </summary>
        /// <param name="id">account_id.</param>
        void AccountInactive(string id);

        /// <summary>
        /// Close account.
        /// </summary>
        /// <param name="id">account_id.</param>
        void AccountClosed(string id);

        /// <summary>
        /// Modify amount of transaction.
        /// </summary>
        /// <param name="id">account_id.</param>
        /// <param name="amount">amount.</param>
        void ModifyTransactionAmount(string id, decimal amount);

        /// <summary>
        /// Dleetion of the account.
        /// </summary>
        /// <param name="id">transaction_id.</param>
        void DeleteAccount(string id);

        /// <summary>
        ///  Delete Account.
        /// </summary>
        /// <param name="id">account_id.</param>
        void DeletePerson(string id);

        /// <summary>
        /// Assign person to account.
        /// </summary>
        /// <param name="personid">person_id.</param>
        /// <param name="accountid">account_id.</param>
        void AssignPersonToAccount(string personid, string accountid);

        /// <summary>
        /// remove the assignment.
        /// </summary>
        /// <param name="personid">person_id.</param>
        /// <param name="accountid">account_id.</param>
        void DesignPersonToAccount(string personid, string accountid);
    }
}
