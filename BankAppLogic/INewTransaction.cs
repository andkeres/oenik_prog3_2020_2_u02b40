﻿// <copyright file="INewTransaction.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankAppLogic
{
    using System;

    /// <summary>
    /// Interface for new transction.
    /// </summary>
    public interface INewTransaction
    {
        /// <summary>
        /// Create new transaction.
        /// </summary>
        /// <param name="id">transaction_id.</param>
        /// <param name="type">transaction_type.</param>
        /// <param name="transfertime">date_of_tranaction.</param>
        /// <param name="amount">transaction_amount.</param>
        /// <param name="currency">transaction_currency.</param>
        /// <param name="accountId">account_id.</param>
        public void CreateNewTransaction(string id, string type, DateTime transfertime, decimal amount, string currency, string accountId);

        /// <summary>
        /// Delete transaction.
        /// </summary>
        /// <param name="id">transaction_id.</param>
        public void DeleteTransaction(string id);
    }
}
