﻿// <copyright file="TestNewAccount.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Teszt
{
    using System;
    using System.IO;
    using BankAppLogic;
    using BankingAppDB.Models;
    using BankingAppRepository2.Repositorii;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Test class for CRUD-Tests.
    /// </summary>
    [TestFixture]
    public class TestNewAccount
    {
        private INewAccount newAccount;
        private Mock<IAccountRepository> accountRepositrymock;
        private Mock<IPersonRepository> personRepositorymock;
        private Mock<IPersonAccount> personaccountRepositorymock;
        private Mock<ITransactionRepository> transactionRepositorymock;

        /// <summary>
        /// Setup of the test environment.
        /// </summary>
        [OneTimeSetUp]

        public void Setup()
        {
            this.personRepositorymock = new Mock<IPersonRepository>();
            this.accountRepositrymock = new Mock<IAccountRepository>();
            this.personaccountRepositorymock = new Mock<IPersonAccount>();
            this.transactionRepositorymock = new Mock<ITransactionRepository>();
            this.newAccount = new NewAccount(this.accountRepositrymock.Object, this.personRepositorymock.Object, this.personaccountRepositorymock.Object);
        }

              /// <summary>
              /// Testing the XXX function.
              /// </summary>
        [Test]
        public void CreateAccountIsWorkingAsExpected()
        {
            // ARRANGE
            Account acc = new Account { Id = "001", IsClosed = false, IsInactive = false, Name = "Account01", Type = "KON" };

            // Do something
            this.newAccount.CreateNewAccount(acc.Id, acc.Name, acc.Type);

            // Assert
            Assert.AreEqual(acc.Id + " " + acc.Name, "001 Account01");
        }
    }
}
