﻿// <copyright file="QueryTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Teszt
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using BankAppLogic;
    using BankAppLogic.Models;
    using BankingAppDB.Models;
    using BankingAppRepository2.Repositorii;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// The neccessary data for test.
    /// </summary>
    [TestFixture]
    public class QueryTest
    {
        // private INewAccount newAccount;
        // private IChangeData changeData;
        // private INewTransaction newTransaction;
        private IDataQuery dataQuery;
        private Mock<IAccountRepository> accountRepositrymock;
        private Mock<IPersonRepository> personRepositorymock;
        private Mock<IPersonAccount> personaccountRepositorymock;
        private Mock<ITransactionRepository> transactionRepositorymock;

        /// <summary>
        /// Setup the environment.
        /// </summary>
        [OneTimeSetUp]
        public void Setup()
        {
            this.personRepositorymock = new Mock<IPersonRepository>();
            this.accountRepositrymock = new Mock<IAccountRepository>();
            this.personaccountRepositorymock = new Mock<IPersonAccount>();
            this.transactionRepositorymock = new Mock<ITransactionRepository>();

            this.dataQuery = new DataQuery(this.personRepositorymock.Object, this.accountRepositrymock.Object, this.transactionRepositorymock.Object, this.personaccountRepositorymock.Object);
        }

        /// <summary>
        /// Tests, if the getAllAccount function i s working properly.
        /// </summary>
        [Test]
        public void GetAllAccountsWorkingAsExpected()
        {
            // Arrange
            List<Account> accounts = new List<Account>()
            {
            new Account() { Id = "001", Name = "Account01", Type = "KON", IsClosed = false, IsInactive = false },
            new Account() { Id = "002", Name = "Account02", Type = "TYP", IsClosed = false, IsInactive = false },
            new Account() { Id = "003", Name = "Account03", Type = "KON", IsClosed = false, IsInactive = false },
            };

            this.accountRepositrymock.Setup(accrepo => accrepo.GetAll()).Returns(accounts.AsQueryable);

            // Do something
            var results = this.dataQuery.GetAllAccount();

            // Assert
            CollectionAssert.AreEqual(results, accounts);
            this.accountRepositrymock.Verify(accrepo => accrepo.GetAll());
        }

        /// <summary>
        /// Tests, if the getAllTransaction function is working properly.
        /// </summary>
        [Test]
        public void GetTransactionssWorkingAsExpected()
        {
            // Arrange
            List<Transaction> transactions = new List<Transaction>()
            {
            new Transaction() { Id = "001", Currency = "Account01", Type = "DIV", Amount = 120000, AccountId = "001" },
            new Transaction() { Id = "002", Currency = "Account02", Type = "DIV", Amount = 250000, AccountId = "001" },
            new Transaction() { Id = "003", Currency = "Account03", Type = "DIV", Amount = 37000, AccountId = "001" },
            };
            this.transactionRepositorymock.Setup(tranzrepo => tranzrepo.GetAll()).Returns(transactions.AsQueryable);

            // Do something
            var results = this.dataQuery.GetTransactions();

            // Assert
            CollectionAssert.AreEqual(results, transactions);
            this.transactionRepositorymock.Verify(transrepo => transrepo.GetAll());
        }

        /// <summary>
        /// Tests, if the SumOfTransactions function is working properly.
        /// </summary>
        [Test]
        public void SumofTransactionWorking()
        {
            // Arrange
            List<Transaction> transactions = new List<Transaction>()
            {
            new Transaction() { Id = "001", Currency = "USD", Type = "DIV", Amount = 120000, AccountId = "001" },
            new Transaction() { Id = "002", Currency = "USD", Type = "DIV", Amount = 250000, AccountId = "001" },
            new Transaction() { Id = "003", Currency = "USD", Type = "DIV", Amount = 37000, AccountId = "002" },
            new Transaction() { Id = "004", Currency = "USD", Type = "DIV", Amount = 37000, AccountId = "002" },
            };

            List<TransactionSumInPeriod> transsum = new List<TransactionSumInPeriod>()
            {
                new TransactionSumInPeriod() { AccountId = "001", Amount = 370000 },
                new TransactionSumInPeriod() { AccountId = "002", Amount = 74000 },
            };
            this.transactionRepositorymock.Setup(tranzrepo => tranzrepo.GetAll()).Returns(transactions.AsQueryable);

            // Do something
            var results = this.dataQuery.SumofAPeriod();

            // Assert
            Assert.That(results.ToString(), Is.EqualTo(transsum.ToString()));

            this.transactionRepositorymock.Verify(transrepo => transrepo.GetAll());
        }

        /// <summary>
        /// Filter of transaction for a given treshold.
        /// </summary>
        [Test]

        public void FilterForThresholdWorking()
        {
            // Arrange
            List<Transaction> transactions = new List<Transaction>()
            {
            new Transaction() { Id = "001", Currency = "USD", Type = "DIV", Amount = 120000, AccountId = "001" },
            new Transaction() { Id = "002", Currency = "USD", Type = "DIV", Amount = 250000, AccountId = "001" },
            new Transaction() { Id = "003", Currency = "USD", Type = "DIV", Amount = 37000, AccountId = "002" },
            new Transaction() { Id = "004", Currency = "USD", Type = "DIV", Amount = 37000, AccountId = "002" },
            };

            List<Account> accounts = new List<Account>()
            {
                new Account() { Id = "001", Name = "Account01", Type = "KON", IsClosed = false, IsInactive = false },
                new Account() { Id = "002", Name = "Account01", Type = "KON", IsClosed = false, IsInactive = false },
            };

            List<AccountToTransaction> transactionrestrict = new List<AccountToTransaction>()
            {
                new AccountToTransaction() { Transactionid = "001", Accountid = "001", Accounttype = "KON", Amount = 120000 },
                new AccountToTransaction() { Transactionid = "002", Accountid = "001", Accounttype = "KON", Amount = 250000 },
            };
            this.transactionRepositorymock.Setup(tranzrepo => tranzrepo.GetAll()).Returns(transactions.AsQueryable);
            this.accountRepositrymock.Setup(accrepo => accrepo.GetAll()).Returns(accounts.AsQueryable);

            // Do something
            var results = this.dataQuery.TransactionsOverTreshold(40000);

            // Assert
            Assert.That(results.ToString(), Is.EqualTo(transactionrestrict.ToString()));

            this.transactionRepositorymock.Verify(transrepo => transrepo.GetAll());
        }

        /// <summary>
        /// Filter of transaction for a given treshold.
        /// </summary>
        [Test]

        public void FilterForAccountIdWorking()
        {
            List<Transaction> transactions = new List<Transaction>()
            {
            new Transaction() { Id = "001", Currency = "USD", Type = "DIV", Amount = 120000, AccountId = "001" },
            new Transaction() { Id = "002", Currency = "USD", Type = "DIV", Amount = 250000, AccountId = "001" },
            new Transaction() { Id = "003", Currency = "USD", Type = "DIV", Amount = 37000, AccountId = "002" },
            new Transaction() { Id = "004", Currency = "USD", Type = "DIV", Amount = 37000, AccountId = "002" },
            };

            List<Transaction> results = new List<Transaction>()
            {
                new Transaction() { Id = "001", Currency = "USD", Type = "DIV", Amount = 120000, AccountId = "001" },
                new Transaction() { Id = "002", Currency = "USD", Type = "DIV", Amount = 250000, AccountId = "001" },
            };

            this.transactionRepositorymock.Setup(tranzrepo => tranzrepo.GetAll()).Returns(transactions.AsQueryable);

            // Do something
            var result = this.dataQuery.TransactionForAnAccount("001");

            // Assert
            Assert.That(result.ToString(), Is.EqualTo(results.ToString()));

            this.transactionRepositorymock.Verify(transrepo => transrepo.GetAll());
        }
    }
}
