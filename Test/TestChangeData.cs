﻿// <copyright file="TestChangeData.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Teszt
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using BankAppLogic;
    using BankingAppDB.Models;
    using BankingAppRepository2.Repositorii;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Test class for non CRUD-Tests.
    /// </summary>
    [TestFixture]
    public class TestChangeData
        {
            private INewAccount newAccount;
            private IChangeData changeData;
            private INewTransaction newTransaction;

            private Mock<IAccountRepository> accountRepositrymock;
            private Mock<IPersonRepository> personRepositorymock;
            private Mock<IPersonAccount> personaccountRepositorymock;
            private Mock<ITransactionRepository> transactionRepositorymock;

        /// <summary>
        /// Setup of testsystem.
        /// </summary>
            [OneTimeSetUp]

            public void Setup()
            {
                this.personRepositorymock = new Mock<IPersonRepository>();
                this.accountRepositrymock = new Mock<IAccountRepository>();
                this.personaccountRepositorymock = new Mock<IPersonAccount>();
                this.transactionRepositorymock = new Mock<ITransactionRepository>();
                this.newAccount = new NewAccount(this.accountRepositrymock.Object, this.personRepositorymock.Object, this.personaccountRepositorymock.Object);
                this.changeData = new ChangeData(this.personRepositorymock.Object, this.accountRepositrymock.Object, this.transactionRepositorymock.Object, this.personaccountRepositorymock.Object);
                this.newTransaction = new NewTransaction(this.transactionRepositorymock.Object);
            }

        /// <summary>
        /// Test for change prson Name.
        /// </summary>
            [Test]
            public void ModifyPersonNameIsWorkingAsExpected()
        {
            // ARRANGE
            List<Person> persons = new List<Person>()
            {
                new Person() { Id = "001", AccountId = 1, Address = "116 BP", AnyjaNeve = "Mothername", Név = "András" },
                new Person() { Id = "002", AccountId = 1, Address = "119 ZG", AnyjaNeve = "Mothername", Név = "Péter" },
            };

            /*List<Person> personsmod = new List<Person>()
            {
                new Person() { Id = "001", AccountId = 1, Address = "116 BP", AnyjaNeve = "Mothername", Név = "Tamás" },
                new Person() { Id = "002", AccountId = 1, Address = "119 ZG", AnyjaNeve = "Mothername", Név = "Péter" },
            };*/

            _ = this.personRepositorymock.Setup(personrepo => personrepo.ChangeName("001", "Tamás"));

            // Do something
            this.changeData.ModifyPersonName("001", "Tamás");

            // * Do something
            // *changeData.ModifyPersonName(per.Id, "Tamás");

            // Assert
            this.personRepositorymock.Verify(persn => persn.ChangeName("001", "Tamás"));
        }

        /// <summary>
        /// Test for transaction deletion.
        /// </summary>
            [Test]
            public void DeleteTransactionWorksAsExpected()
        { // ARRANGE
            List<Transaction> transactions = new List<Transaction>()
            {
            new Transaction() { Id = "001", Currency = "Account01", Type = "DIV", Amount = 120000, AccountId = "001" },
            new Transaction() { Id = "002", Currency = "Account02", Type = "DIV", Amount = 250000, AccountId = "001" },
            new Transaction() { Id = "003", Currency = "Account03", Type = "DIV", Amount = 37000, AccountId = "001" },
            };
            this.transactionRepositorymock.Setup(transrepo => transrepo.GetByID("003")).Equals(null);

            // Do something
            this.newTransaction.DeleteTransaction("003");

            // Assert
            this.transactionRepositorymock.Verify(tranzrepo => tranzrepo.GetByID("003"));
        }
    }
}
