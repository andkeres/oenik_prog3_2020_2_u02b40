﻿// <copyright file="Operationen.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankingAppplication
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using BankAppLogic;
    using BankingAppDB;
    using BankingAppRepository2.Repositorii;

    /// <summary>
    /// Extension method.
    /// </summary>
    public static class Operationen
    {
        /// <summary>
        /// Writes the input to the console.
        /// </summary>
        /// <typeparam name="T">text.</typeparam>
        /// <param name="input">input.</param>
        /// <param name="str">string.</param>
        /// <returns>T generic.</returns>
        public static T ToConsole<T>(this IEnumerable<T> input, string str)
        {
            Console.WriteLine($"*************************" + str);
            foreach (var item in input)
            {
                    Console.WriteLine(item.ToString());
            }

            T a1 = (T)input;
            Console.WriteLine($"*************************" + str);
            Console.ReadLine();
            return a1;
        }
    }
}