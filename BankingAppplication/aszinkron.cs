﻿// <copyright file="aszinkron.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankingAppplication
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using BankAppLogic;
    using BankAppLogic.Models;
    using BankingAppDB.Models;

    /// <summary>
    /// Asyncron processes.
    /// </summary>
    public static class Aszinkron
    {
        /// <summary>
        /// PersonAccountResultJoinUsingTask.
        /// </summary>
        /// <param name="logica">logica.</param>
        public static void PersonAccountResultJoinUsingTask(DataQuery logica)
        {
            Task<ICollection<PersonAccount>> t = logica.GetPersonToAccountAsync();
            Console.WriteLine("GetPersonToAccountAsync as task1 started");
            t.Wait();
            ICollection<PersonAccount> avList = t.Result;
            foreach (var item in avList)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }

        /// <summary>
        /// AccountToTransactionResult.
        /// </summary>
        /// <param name="logika">logika.</param>
        public static void AccountToTransactionResultJoinUsingTask(DataQuery logika)
        {
            Task<IList<AccountToTransaction>> t = logika.TransactionsOverTresholdAsync(null);
            Console.WriteLine("TransactionsOverTresholdAsync as task1 started");
            t.Wait();
            ICollection<AccountToTransaction> avList = t.Result;
            foreach (var item in avList)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }

        /// <summary>
        /// SumofAPeriodAsyncJoinUsingTask.
        /// </summary>
        /// <param name="logic">logic.</param>
        public static void SumofAPeriodAsyncJoinUsingTask(DataQuery logic)
        {
            Task<IList<TransactionSumInPeriod>> t = logic.SumofAPeriodAsync();
            Console.WriteLine("SumofAPeriodAsync as task1 started");
            t.Wait();
            ICollection<TransactionSumInPeriod> avList = t.Result;
            foreach (var item in avList)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }

        /// <summary>
        /// SumofAPeriodAsyncJoinUsingTask.
        /// </summary>
        /// <param name="dataQuery">dataquery.</param>
        internal static void SumofAPeriodAsyncJoinUsingTask(IDataQuery dataQuery)
        {
            throw new NotImplementedException();
        }
    }
}
