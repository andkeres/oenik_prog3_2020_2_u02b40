﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BankingAppplication
{
    using System;
    using System.Collections.Generic;
    using BankAppLogic;
    using BankingAppDB;
    using BankingAppRepository2.Repositorii;

    /// <summary>
    /// The class Program.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The menupoints.
        /// </summary>
        public enum Menupoint
        {
            /// <summary>
            /// Modify Account
            /// </summary>
            ModifyAccount = 1,

            /// <summary>
            /// Create New Account
            /// </summary>
            CreateNewAccount = 2,

            /// <summary>
            /// Create New Transaction
            /// </summary>
            NewTarnsacion = 3,

            /// <summary>
            /// Querries
            /// </summary>
            AccountQuery = 4,
        }

        /// <summary>
        /// még nincsen készen.
        /// </summary>
        public static void Main()
        {
            using (var ctx = new PersonsDBContext())
            {
                IPersonRepository personrepo = new PersonRepository(ctx);
                IAccountRepository accountrepo = new AccountRepository(ctx);
                ITransactionRepository transactionrepo = new TransactionRepository(ctx);
                PersonAccountRepository peracc = new PersonAccountRepository(ctx);

                IChangeData changeData = new ChangeData(personrepo, accountrepo, transactionrepo, peracc);
                IDataQuery dataQuery = new DataQuery(personrepo, accountrepo, transactionrepo, peracc);
                INewAccount newAccount = new NewAccount(accountrepo, personrepo, peracc);
                INewTransaction newTransaction = new NewTransaction(transactionrepo);

                bool done = false;
                do
                {
                    Console.Clear();
                    Console.WriteLine("Welcome to the Banking application of Cash & Co.");
                    Console.WriteLine("Choose an option!");
                    Console.WriteLine("1) Modify Account");
                    Console.WriteLine("2) Create new Account");
                    Console.WriteLine("3) New Transaction ");
                    Console.WriteLine("4) Query for you account");
                    Console.WriteLine("5) Modify Person");

                    int menu = int.Parse(Console.ReadLine());

                    switch (menu)
                    {
                        case 1:
                            Console.WriteLine("Please enter the Id of the Account you wish to modify");
                            Console.WriteLine("Id: "); string id = Console.ReadLine();

                            Console.WriteLine("What would you like to do?");
                            Console.WriteLine("1 - Delete");
                            Console.WriteLine("2 - Modify Name");
                            Console.WriteLine("3 - Set status to inactive");
                            Console.WriteLine("4 - Set status to closed");
                            Console.WriteLine("5 - Assign account to a person");
                            Console.WriteLine("6  -  Unassigned person");
                            int menu12 = int.Parse(Console.ReadLine());
                            switch (menu12)
                            {
                                case 1:
                                    changeData.DeleteAccount(id);
                                    break;

                                case 2:
                                    Console.WriteLine("Please enter the new name of the account: ");
                                    Console.WriteLine("Name: "); string name11 = Console.ReadLine();
                                    changeData.ModifyAccountName(id, name11);
                                    break;

                                case 3:
                                    changeData.AccountInactive(id);
                                    break;
                                default:
                                case 4:
                                    changeData.AccountClosed(id);
                                    break;
                                case 5:
                                    Console.WriteLine("please enter the Id of the person: "); string personId = Console.ReadLine();
                                    changeData.AssignPersonToAccount(personId, id);
                                    break;
                                case 6:
                                    Console.WriteLine("please enter the Id of the account: "); string personId2 = Console.ReadLine();
                                    changeData.DesignPersonToAccount(personId2, id);
                                    break;
                            }

                            break;

                        case 2:
                            Console.WriteLine("Please enter the details of the the account you wish to create:");
                            Console.WriteLine("Id: "); string id1 = Console.ReadLine();
                            Console.WriteLine("Name: "); string name1 = Console.ReadLine();
                            Console.WriteLine("Type: "); string type = Console.ReadLine();
                            newAccount.CreateNewAccount(id1, name1, type);

                            Console.WriteLine("Please enter the detaials of the person you wish to create:");
                            Console.WriteLine("Id: "); string id2 = Console.ReadLine();
                            Console.WriteLine("Name: "); string name2 = Console.ReadLine();
                            Console.WriteLine("Date of Birth: "); DateTime dob = DateTime.Parse(Console.ReadLine());
                            Console.WriteLine("Address: "); string address1 = Console.ReadLine();
                            Console.WriteLine("Mother name"); string mothername = Console.ReadLine();
                            newAccount.CreateNewPerson(id2, name2, mothername, address1, dob);

                            Console.WriteLine("Would you like to assign the person to the account? (Y / N)");
                            string answer = Console.ReadLine();
                            if (answer == "Y")
                            {
                                changeData.AssignPersonToAccount(id2, id1);
                            }

                            break;
                        case 3:
                            Console.WriteLine("Please choose one of the following options: ");
                            Console.WriteLine("1. - Crate a new transaction");
                            Console.WriteLine("2. - Delete a transaction");
                            Console.WriteLine("3. - Modify the sum of a recent transaction");
                            int menu2 = int.Parse(Console.ReadLine());

                            switch (menu2)
                            {
                                case 1:
                                    Console.WriteLine("Please enter the details of the the transaction you wish to create:");
                                    Console.WriteLine("Id: "); string id3 = Console.ReadLine();
                                    Console.WriteLine("Amount: "); decimal amount = decimal.Parse(Console.ReadLine());
                                    Console.WriteLine("Currency: "); string currency = Console.ReadLine();
                                    Console.WriteLine("AccountId: "); string accounId = Console.ReadLine();
                                    Console.WriteLine("Type: "); string type2 = Console.ReadLine();

                                    newTransaction.CreateNewTransaction(id3, type2, DateTime.Now, amount, currency, accounId);
                                    break;
                                case 2:
                                    Console.WriteLine("Please enter the Id of the transaction you wish to delete:");
                                    string id41 = Console.ReadLine();

                                    newTransaction.DeleteTransaction(id41);
                                    break;
                                case 3:
                                    Console.WriteLine("Please enter the id of the transaction, and the new sum");
                                    string id4 = Console.ReadLine();
                                    decimal newsum = decimal.Parse(Console.ReadLine());

                                    changeData.ModifyTransactionAmount(id4, newsum);

                                    break;
                            }

                            break;
                        case 4:
                            Console.WriteLine("Which data would you like to query from the transaction history? Please, choose one: ");
                            Console.WriteLine("1  -  List all Persons");
                            Console.WriteLine("2  -  List all Accounts");
                            Console.WriteLine("3  -  List all Transactions");
                            Console.WriteLine("4  -  List all Person-Account relations ");
                            Console.WriteLine("5  -  List all Transactions for an account");
                            Console.WriteLine("6  -  List all Transactions for an account, ASYNC");
                            Console.WriteLine("7  -  List Transactions with Account details over a treshold");
                            Console.WriteLine("8  -  List Transactions with Account details over a treshold, ASNC");
                            Console.WriteLine("9   - Sum of transaction amouns for accounts");
                            Console.WriteLine("10   - Sum of transaction amouns for accounts, ASYNC");

                            int menu3 = int.Parse(Console.ReadLine());

                            switch (menu3)
                            {
                                case 1:
                                    dataQuery.GetAllPersons().ToConsole("all persons");
                                    break;
                                case 2:
                                    dataQuery.GetAllAccount().ToConsole("all accounts");

                                    break;
                                case 3:
                                    dataQuery.GetTransactions().ToConsole("all tarnsactions");
                                    break;
                                case 4:
                                    dataQuery.GetPersonToAccount().ToConsole("all person-account relationships");
                                    break;
                                case 5:
                                    Console.WriteLine("Please enter the Id of the account");
                                    Console.WriteLine("Id: "); string id4 = Console.ReadLine();
                                    dataQuery.TransactionForAnAccount(id4).ToConsole("all tarnsactions");
                                    break;
                                case 6:
                                    Aszinkron.PersonAccountResultJoinUsingTask((DataQuery)dataQuery);
                                    break;
                                case 7:
                                    Console.WriteLine("Please enter the treshold value");
                                    Console.WriteLine("Treshold: "); decimal treshold = decimal.Parse(Console.ReadLine());
                                    dataQuery.TransactionsOverTreshold(treshold).ToConsole("all tarnsactions");
                                    break;
                                case 8:
                                    Aszinkron.AccountToTransactionResultJoinUsingTask((DataQuery)dataQuery);
                                    break;
                                case 9:
                                    Console.WriteLine("Please enter the begin and the end of the date period in YYYY-MM-DD format");
                                    /*Console.WriteLine("Begin of period: "); DateTime begin = DateTime.Parse(Console.ReadLine());
                                    Console.WriteLine("Begin of period: "); DateTime end = DateTime.Parse(Console.ReadLine());*/
                                    dataQuery.SumofAPeriod().ToConsole("transaction amount aggregated");
                                    Console.ReadLine();
                                    break;
                                case 10:
                                    Aszinkron.SumofAPeriodAsyncJoinUsingTask(dataQuery);
                                    break;
                            }

                            break;

                        case 5:
                            Console.WriteLine("Please enter the Id of the person, you wish to modify: ");
                            Console.WriteLine("Id: "); string id5 = Console.ReadLine();

                            Console.WriteLine("What would you like to do?");
                            Console.WriteLine("1 - Delete");
                            Console.WriteLine("2 - Modify Address");
                            Console.WriteLine("3 - Modify Name");
                            Console.WriteLine("4 - Assign to Account");
                            Console.WriteLine("5 - Delete assignment to Account");

                            int menu13 = int.Parse(Console.ReadLine());

                            switch (menu13)
                            {
                                case 1:
                                    changeData.DeletePerson(id5);
                                    break;

                                case 2:
                                    Console.WriteLine("Please, enter the new address of your person");
                                    string address = Console.ReadLine();
                                    changeData.ModifyPersonAddress(id5, address);
                                    break;

                                case 3:
                                    Console.WriteLine("Please, enter the new name of your person");
                                    string name = Console.ReadLine();
                                    changeData.ModifyPersonName(id5, name);
                                    break;
                                case 4:
                                    Console.WriteLine("please enter the Id of the account: "); string accountId = Console.ReadLine();
                                    changeData.AssignPersonToAccount(id5, accountId);
                                    break;
                                case 5:
                                    Console.WriteLine("please enter the Id of the account: "); string accountId2 = Console.ReadLine();
                                    changeData.DesignPersonToAccount(id5, accountId2);
                                    break;
                            }

                            break;
                    }
                }
                while (!done);
        }
        }
    }
}