﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>", Scope = "member", Target = "~M:BankingAppplication.Operationen.Main")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>", Scope = "member", Target = "~M:BankingAppplication.Operationen.Main")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:BankingAppplication.Operations.ToConsole``1(System.Collections.Generic.IEnumerable{``0},System.String)")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1404:Code analysis suppression should have justification", Justification = "<Pending>")]

[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:BankingAppplication.Operation.ToConsole``1(System.Collections.Generic.IEnumerable{``0},System.String)")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:BankingAppplication.Operationen.ToConsole``1(System.Collections.Generic.IEnumerable{``0},System.String)")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>", Scope = "member", Target = "~M:BankingAppplication.Program.Main")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>", Scope = "member", Target = "~M:BankingAppplication.Program.Main")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>", Scope = "member", Target = "~M:BankingAppplication.Aszinkron.PersonAccountResultJoinUsingTask(BankAppLogic.DataQuery)")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>", Scope = "member", Target = "~M:BankingAppplication.Aszinkron.AccountToTransactionResultJoinUsingTask(BankAppLogic.DataQuery)")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>", Scope = "member", Target = "~M:BankingAppplication.Aszinkron.SumofAPeriodAsyncJoinUsingTask(BankAppLogic.DataQuery)")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:BankingAppplication.Aszinkron.SumofAPeriodAsyncJoinUsingTask(BankAppLogic.DataQuery)")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:BankingAppplication.Aszinkron.AccountToTransactionResultJoinUsingTask(BankAppLogic.DataQuery)")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:BankingAppplication.Aszinkron.PersonAccountResultJoinUsingTask(BankAppLogic.DataQuery)")]
[assembly: SuppressMessage("Style", "IDE0044:Add readonly modifier", Justification = "<Pending>", Scope = "member", Target = "~F:BankingAppplication.Aszinkron.threshold")]
